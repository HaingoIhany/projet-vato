<%-- Document : insertVopoka Created on : 22 nov. 2022, 22:04:37 Author : Haingo --%>

    <%@page import="java.text.DecimalFormat" %>
        <%@page import="model.Prediction" %>
            <%@page import="java.util.List" %>
                <%@page import="model.Lavaka" %>
                    <%@page contentType="text/html" pageEncoding="UTF-8" %>
                        <!DOCTYPE html>
                        <html>

                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                            <link rel="stylesheet" href="style.css">
                            <title>JSP Page</title>
                        </head>

                        <body>

                            <%Lavaka[] lLavaka=(Lavaka[]) request.getAttribute("lLavaka");%>
                                <%List<Prediction> efficacitePJ = (List<Prediction>)
                                        request.getAttribute("efficacitePJ");%>
                                        <%List<Prediction> props = (List<Prediction>) request.getAttribute("props");%>
                                                <%List<Prediction> durete = (List<Prediction>)
                                                        request.getAttribute("durete");%>
                                                        <%List<Prediction> exploitation = (List<Prediction>)
                                                                request.getAttribute("exploitation");%>
                                                                <%DecimalFormat df=new DecimalFormat("#.##");%>

                                                                    <div class="fonony"
                                                                        style="display: flex; flex-direction: column; align-items: center; height: 100%; align-content: space-around; gap: 2rem; justify-content: center;">
                                                                        <div style="box-shadow: 5px 10px #88fd5e;">
                                                                            <form action="InsertResultatCTRL"
                                                                                method="post">
                                                                                Vato : <input type="text" name="qteInit"
                                                                                    placeholder="en tonne">
                                                                                <br>
                                                                                Fasika : <input type="text"
                                                                                    name="fasika" placeholder="en kg">
                                                                                <br>
                                                                                Volamena : <input type="text"
                                                                                    name="volamena"
                                                                                    placeholder="ex: 0.003 (en g)">
                                                                                <br>
                                                                                Litre fitotona : <input type="text"
                                                                                    name="pf"
                                                                                    placeholder="ex: 2 (en l/h)">
                                                                                <br>
                                                                                Prix volamena : <input type="text"
                                                                                    name="pv"
                                                                                    placeholder="ex: 10000 (en ar/g)">
                                                                                <br>
                                                                                Gasoil : <input type="text" name="g"
                                                                                    value="4900"
                                                                                    placeholder="ex: 4900 (en ar)">
                                                                                <br>
                                                                                Lavaka : <select name="idLavaka">
                                                                                    <% for (int i=0; i < lLavaka.length;
                                                                                        i++) {%>
                                                                                        <option
                                                                                            value="<%=lLavaka[i].getId()%>">
                                                                                            <%=lLavaka[i].getNom()%>
                                                                                        </option>
                                                                                        <% }%>
                                                                                </select>
                                                                                <br>
                                                                                Date debut : <input
                                                                                    type="datetime-local" name="dd">
                                                                                <br>
                                                                                Date fin : <input type="datetime-local"
                                                                                    name="df">
                                                                                <br>
                                                                                <input type="submit" value="Traiter" />
                                                                            </form>
                                                                        </div>

                                                                        <div style="box-shadow: 5px 10px fuchsia;">
                                                                            <h2>Efficacite par jour</h2>
                                                                            <table border="1px">
                                                                                <tr>
                                                                                    <th>idLavaka</th>
                                                                                    <th>mpfPH</th>
                                                                                    <th>pvPJ</th>
                                                                                    <th>mmitotoPH</th>
                                                                                    <th>mpropGT</th>
                                                                                    <th>mduretePH</th>
                                                                                    <th>efficacitePJ</th>
                                                                                    <th>exploitation</th>
                                                                                </tr>
                                                                                <% for (Prediction p : efficacitePJ) {%>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <%= p.getIdLavaka()%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMpfPH())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%= df.format(p.getPvPJ())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMmitotoPH())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMpropGT())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%= p.getMduretePH()%>
                                                                                        </td>
                                                                                        <td
                                                                                            style="background-color: blue;">
                                                                                            <%=
                                                                                                df.format(p.getEfficacitePJ())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getExploitation())%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <% } %>
                                                                            </table>
                                                                        </div>

                                                                        <div style="box-shadow: 5px 10px cyan;">
                                                                            <h2>Proportion</h2>
                                                                            <table border="1px">
                                                                                <tr>
                                                                                    <th>idLavaka</th>
                                                                                    <th>mpfPH</th>
                                                                                    <th>pvPJ</th>
                                                                                    <th>mmitotoPH</th>
                                                                                    <th>mpropGT</th>
                                                                                    <th>mduretePH</th>
                                                                                    <th>efficacitePJ</th>
                                                                                    <th>exploitation</th>
                                                                                </tr>
                                                                                <% for (Prediction p : props) {%>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <%= p.getIdLavaka()%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMpfPH())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%= df.format(p.getPvPJ())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMmitotoPH())%>
                                                                                        </td>
                                                                                        <td
                                                                                            style="background-color: blue;">
                                                                                            <%=
                                                                                                df.format(p.getMpropGT())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%= p.getMduretePH()%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getEfficacitePJ())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getExploitation())%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <% } %>
                                                                            </table>
                                                                        </div>
                                                                        <div style="box-shadow: 5px 10px red;">
                                                                            <h2>Durete</h2>
                                                                            <table border="1px">
                                                                                <tr>
                                                                                    <th>idLavaka</th>
                                                                                    <th>mpfPH</th>
                                                                                    <th>pvPJ</th>
                                                                                    <th>mmitotoPH</th>
                                                                                    <th>mpropGT</th>
                                                                                    <th>mduretePH</th>
                                                                                    <th>efficacitePJ</th>
                                                                                    <th>exploitation</th>
                                                                                </tr>
                                                                                <% for (Prediction p : durete) {%>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <%= p.getIdLavaka()%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMpfPH())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%= df.format(p.getPvPJ())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMmitotoPH())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMpropGT())%>
                                                                                        </td>
                                                                                        <td
                                                                                            style="background-color: blue;">
                                                                                            <%= p.getMduretePH()%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getEfficacitePJ())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getExploitation())%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <% }%>
                                                                            </table>
                                                                        </div>

                                                                        <div style="box-shadow: 5px 10px purple;">
                                                                            <h2>Exploitation</h2>
                                                                            <table border="1px">
                                                                                <tr>
                                                                                    <th>idLavaka</th>
                                                                                    <th>mpfPH</th>
                                                                                    <th>pvPJ</th>
                                                                                    <th>mmitotoPH</th>
                                                                                    <th>mpropGT</th>
                                                                                    <th>mduretePH</th>
                                                                                    <th>efficacitePJ</th>
                                                                                    <th>exploitation</th>
                                                                                </tr>
                                                                                <% for (Prediction p : durete) {%>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <%= p.getIdLavaka()%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMpfPH())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%= df.format(p.getPvPJ())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMmitotoPH())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getMpropGT())%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%= p.getMduretePH()%>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%=
                                                                                                df.format(p.getEfficacitePJ())%>
                                                                                        </td>
                                                                                        <td
                                                                                            style="background-color: blue;">
                                                                                            <%=
                                                                                                df.format(p.getExploitation())%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <% }%>
                                                                            </table>
                                                                        </div>
                                                                        <br>

                                                                        <button><a href="VoirGrapheCTRL">Voir
                                                                                Graphe</a></button>
                                                                    </div>


                        </body>

                        </html>