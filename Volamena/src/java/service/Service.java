/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Carriere;
import model.Lavaka;
import model.Prediction;
import model.Resultat;
import utilitaire.Connexion;
import utilitaire.DBTable;

/**
 *
 * @author Haingo
 */
public class Service {

    public Carriere[] getCarriere() throws Exception {
        Connexion con = new Connexion();
        try {
            String req = "select * from Carriere";
            Carriere temp = new Carriere();
            DBTable[] db = temp.find2(req, con.getCon());
            Carriere[] result = new Carriere[db.length];
            for (int i = 0; i < db.length; i++) {
                result[i] = (Carriere) db[i];
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            con.close();
        }
    }

    public Lavaka[] getLavaka() throws Exception {
        Connexion con = new Connexion();
        try {
            String req = "select * from Lavaka";
            Lavaka temp = new Lavaka();
            DBTable[] db = temp.find2(req, con.getCon());
            Lavaka[] result = new Lavaka[db.length];
            for (int i = 0; i < db.length; i++) {
                result[i] = (Lavaka) db[i];
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            con.close();
        }
    }

    public Lavaka getOneLavaka(int idLavaka) throws Exception {
        Connexion con = new Connexion();
        try {
            String req = "select * from Lavaka where id = " + idLavaka + "";
            Lavaka temp = new Lavaka();
            DBTable[] db = temp.find2(req, con.getCon());
            Lavaka[] result = new Lavaka[db.length];
            for (int i = 0; i < db.length; i++) {
                result[i] = (Lavaka) db[i];
            }
            return result[0];
        } catch (Exception ex) {
            throw ex;
        } finally {
            con.close();
        }
    }

    public List<Resultat> getResultat() throws Exception {
        Connexion con = new Connexion();
        List<Resultat> lf = new ArrayList<>();
        try {
            String req = "select * from Resultat";
            Resultat temp = new Resultat();

            Statement st = con.getCon().createStatement();
            ResultSet rs = st.executeQuery(req);

            while (rs.next()) {
                Resultat v = new Resultat(
                        rs.getInt("id"),
                        rs.getDouble("pf"),
                        rs.getDouble("pv"),
                        rs.getDouble("g"),
                        rs.getDouble("qteInit"),
                        rs.getInt("idLavaka"),
                        rs.getTimestamp("dateDebut").toLocalDateTime(),
                        rs.getTimestamp("dateFin").toLocalDateTime(),
                        rs.getInt("duree"),
                        rs.getDouble("fasika"),
                        rs.getDouble("volamena")
                );
                lf.add(v);
            }
            st.close();
            return lf;
        } catch (Exception ex) {
            throw ex;
        } finally {
            con.close();
        }
    }

    public List<Prediction> getPrediction() throws Exception {
        Connexion con = new Connexion();
        List<Prediction> lf = new ArrayList<>();
        try {
            String req = "SELECT 	idLavaka, AVG(pfPH) mpfPH, (((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) pvPJ, AVG(mitotoPH) mmitotoPH, AVG(propGT) mpropGT,  \n"
                    + "		AVG(duretePH) mduretePH, ((AVG(mitotoPH) * AVG(propGT)) * 8) efficacitePJ, ((((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) - AVG(pfPH)) exploitation\n"
                    + "FROM vUnitaire\n"
                    + "GROUP BY idLavaka\n"
                    + "ORDER BY ((AVG(mitotoPH) * AVG(propGT)) * 8) DESC";

            Statement st = con.getCon().createStatement();
            ResultSet rs = st.executeQuery(req);

            while (rs.next()) {
                Prediction v = new Prediction(
                        rs.getInt("idLavaka"),
                        rs.getDouble("mpfPH"),
                        rs.getDouble("pvPJ"),
                        rs.getDouble("mmitotoPH"),
                        rs.getDouble("mpropGT"),
                        rs.getDouble("mduretePH"),
                        rs.getDouble("efficacitePJ"),
                        rs.getDouble("exploitation")
                );
                lf.add(v);
            }
            st.close();
            return lf;
        } catch (Exception ex) {
            throw ex;
        } finally {
            con.close();
        }
    }

    public List<Prediction> getProps() throws Exception {
        Connexion con = new Connexion();
        List<Prediction> lf = new ArrayList<>();
        try {
            String req = "SELECT 	idLavaka, AVG(pfPH) mpfPH, (((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) pvPJ, AVG(mitotoPH) mmitotoPH, AVG(propGT) mpropGT,  \n"
                    + "		AVG(duretePH) mduretePH, ((AVG(mitotoPH) * AVG(propGT)) * 8) efficacitePJ, ((((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) - AVG(pfPH)) exploitation\n"
                    + "FROM vUnitaire\n"
                    + "GROUP BY idLavaka\n"
                    + "ORDER BY AVG(propGT) DESC";

            Statement st = con.getCon().createStatement();
            ResultSet rs = st.executeQuery(req);

            while (rs.next()) {
                Prediction v = new Prediction(
                        rs.getInt("idLavaka"),
                        rs.getDouble("mpfPH"),
                        rs.getDouble("pvPJ"),
                        rs.getDouble("mmitotoPH"),
                        rs.getDouble("mpropGT"),
                        rs.getDouble("mduretePH"),
                        rs.getDouble("efficacitePJ"),
                        rs.getDouble("exploitation")
                );
                lf.add(v);
            }
            st.close();
            return lf;
        } catch (Exception ex) {
            throw ex;
        } finally {
            con.close();
        }
    }

    public List<Prediction> getDurete() throws Exception {
        Connexion con = new Connexion();
        List<Prediction> lf = new ArrayList<>();
        try {
            String req = "SELECT 	idLavaka, AVG(pfPH) mpfPH, (((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) pvPJ, AVG(mitotoPH) mmitotoPH, AVG(propGT) mpropGT,  \n"
                    + "		AVG(duretePH) mduretePH, ((AVG(mitotoPH) * AVG(propGT)) * 8) efficacitePJ, ((((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) - AVG(pfPH)) exploitation\n"
                    + "FROM vUnitaire\n"
                    + "GROUP BY idLavaka\n"
                    + "ORDER BY AVG(duretePH) ASC";

            Statement st = con.getCon().createStatement();
            ResultSet rs = st.executeQuery(req);

            while (rs.next()) {
                Prediction v = new Prediction(
                        rs.getInt("idLavaka"),
                        rs.getDouble("mpfPH"),
                        rs.getDouble("pvPJ"),
                        rs.getDouble("mmitotoPH"),
                        rs.getDouble("mpropGT"),
                        rs.getDouble("mduretePH"),
                        rs.getDouble("efficacitePJ"),
                        rs.getDouble("exploitation")
                );
                lf.add(v);
            }
            st.close();
            return lf;
        } catch (Exception ex) {
            throw ex;
        } finally {
            con.close();
        }
    }

    public List<Prediction> getExploitation() throws Exception {
        Connexion con = new Connexion();
        List<Prediction> lf = new ArrayList<>();
        try {
            String req = "SELECT 	idLavaka, AVG(pfPH) mpfPH, (((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) pvPJ, AVG(mitotoPH) mmitotoPH, AVG(propGT) mpropGT,  \n"
                    + "		AVG(duretePH) mduretePH, ((AVG(mitotoPH) * AVG(propGT)) * 8) efficacitePJ, ((((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) - AVG(pfPH)) exploitation\n"
                    + "FROM vUnitaire\n"
                    + "GROUP BY idLavaka\n"
                    + "ORDER BY ((((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) - AVG(pfPH)) DESC";

            Statement st = con.getCon().createStatement();
            ResultSet rs = st.executeQuery(req);

            while (rs.next()) {
                Prediction v = new Prediction(
                        rs.getInt("idLavaka"),
                        rs.getDouble("mpfPH"),
                        rs.getDouble("pvPJ"),
                        rs.getDouble("mmitotoPH"),
                        rs.getDouble("mpropGT"),
                        rs.getDouble("mduretePH"),
                        rs.getDouble("efficacitePJ"),
                        rs.getDouble("exploitation")
                );
                lf.add(v);
            }
            st.close();
            return lf;
        } catch (Exception ex) {
            throw ex;
        } finally {
            con.close();
        }
    }

    public void insertResultat(Resultat r) throws Exception {
        Connexion con = new Connexion();
        try {
            Resultat temp = new Resultat(1, r.getPf(), r.getPv(), r.getG(), r.getQteInit(), r.getIdLavaka(), r.getDateDebut(), r.getDateFin(), r.getDuree(), r.getFasika(), r.getVolamena());
            temp.insert(con.getCon());
        } catch (Exception ex) {
            throw ex;
        } finally {
            con.close();
        }
    }
}
