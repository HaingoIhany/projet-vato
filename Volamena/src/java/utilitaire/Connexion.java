/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitaire;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Haingo
 */
public class Connexion {
    Connection con;

    public Connexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/volamena","root","root");
        } catch (ClassNotFoundException | SQLException e) {
        }
    }

    public Connection getCon() {
        return con;
    }

    public void close() {
        try {
            getCon().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
