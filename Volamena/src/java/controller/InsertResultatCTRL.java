/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Lavaka;
import model.Prediction;
import model.Resultat;
import service.Service;

/**
 *
 * @author Haingo
 */
public class InsertResultatCTRL extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        double qteInit = Double.parseDouble(request.getParameter("qteInit"));
        double pf = Double.parseDouble(request.getParameter("pf"));
        double pv = Double.parseDouble(request.getParameter("pv"));
        double g = Double.parseDouble(request.getParameter("g"));
        double fasika = Double.parseDouble(request.getParameter("fasika"));
        double volamena = Double.parseDouble(request.getParameter("volamena"));
        int idLavaka = Integer.parseInt(request.getParameter("idLavaka"));
        String ddt = request.getParameter("dd");
        String dft = request.getParameter("df");

        out.println(qteInit);
        out.println(idLavaka);
        out.println(ddt);
        out.println(dft);
        out.println(fasika);

        Service s = new Service();
        double diffHeure = 0;
        double diffHeurem = 0;
        long millis = Duration.between(LocalDateTime.parse(ddt), LocalDateTime.parse(dft)).toMillis();
        diffHeurem = (TimeUnit.MILLISECONDS.toMinutes(millis));
        diffHeure = diffHeurem / 60;
        out.println(diffHeure);
        
//        Lavaka lavakaENC = s.getOneLavaka(idLavaka);
//        out.println(lavakaENC.getProportion());

        Resultat temp = new Resultat(1, pf, pv, g, qteInit, idLavaka, LocalDateTime.parse(ddt), LocalDateTime.parse(dft), diffHeure, fasika, volamena);
        s.insertResultat(temp);

        // Miverina amle page
        Service mt = new Service();
        Lavaka[] lLavaka = mt.getLavaka();
        List<Prediction> efficacitePJ = mt.getPrediction();
        List<Prediction> props = mt.getProps();
        List<Prediction> durete = mt.getDurete();
        List<Prediction> exploitation = mt.getExploitation();
        

        request.setAttribute("lLavaka", lLavaka);
        request.setAttribute("efficacitePJ", efficacitePJ);
        request.setAttribute("props", props);
        request.setAttribute("durete", durete);
        request.setAttribute("exploitation", exploitation);

        RequestDispatcher rd = request.getRequestDispatcher("view/traitement.jsp");
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(InsertResultatCTRL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(InsertResultatCTRL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
