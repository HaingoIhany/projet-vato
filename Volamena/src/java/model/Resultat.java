/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDateTime;
import utilitaire.DBTable;

/**
 *
 * @author Haingo
 */
public class Resultat extends DBTable {
    private int id;
    private double pf;
    private double pv;
    private double g;
    private double qteInit;
    private int idLavaka;
    private LocalDateTime dateDebut;
    private LocalDateTime dateFin;
    private double duree;
    private double fasika;
    private double volamena;

    public Resultat() {
    }

    public Resultat(int id, double pf, double pv, double g, double qteInit, int idLavaka, LocalDateTime dateDebut, LocalDateTime dateFin, double duree, double fasika, double volamena) {
        this.id = id;
        this.pf = pf;
        this.pv = pv;
        this.g = g;
        this.qteInit = qteInit;
        this.idLavaka = idLavaka;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.duree = duree;
        this.fasika = fasika;
        this.volamena = volamena;
    }

    public double getPf() {
        return pf;
    }

    public void setPf(double pf) {
        this.pf = pf;
    }

    public double getPv() {
        return pv;
    }

    public void setPv(double pv) {
        this.pv = pv;
    }

    public double getG() {
        return g;
    }

    public void setG(double g) {
        this.g = g;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getQteInit() {
        return qteInit;
    }

    public void setQteInit(double qteInit) {
        this.qteInit = qteInit;
    }

    public int getIdLavaka() {
        return idLavaka;
    }

    public void setIdLavaka(int idLavaka) {
        this.idLavaka = idLavaka;
    }

    public LocalDateTime getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDateTime dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDateTime getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDateTime dateFin) {
        this.dateFin = dateFin;
    }

    public double getDuree() {
        return duree;
    }

    public void setDuree(double duree) {
        this.duree = duree;
    }

    public double getFasika() {
        return fasika;
    }

    public void setFasika(double fasika) {
        this.fasika = fasika;
    }

    public double getVolamena() {
        return volamena;
    }

    public void setVolamena(double volamena) {
        this.volamena = volamena;
    }

}
