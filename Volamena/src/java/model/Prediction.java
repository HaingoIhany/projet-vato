/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Haingo
 */
public class Prediction {
    private int idLavaka;
    private double mpfPH;
    private double pvPJ;
    private double mmitotoPH;
    private double mpropGT;
    private double mduretePH;
    private double efficacitePJ;
    private double exploitation;

    public Prediction() {
    }

    public Prediction(int idLavaka, double mpfPH, double pvPJ, double mmitotoPH, double mpropGT, double mduretePH, double efficacitePJ, double exploitation) {
        this.idLavaka = idLavaka;
        this.mpfPH = mpfPH;
        this.pvPJ = pvPJ;
        this.mmitotoPH = mmitotoPH;
        this.mpropGT = mpropGT;
        this.mduretePH = mduretePH;
        this.efficacitePJ = efficacitePJ;
        this.exploitation = exploitation;
    }

    public double getExploitation() {
        return exploitation;
    }

    public void setExploitation(double exploitation) {
        this.exploitation = exploitation;
    }

    public double getMpfPH() {
        return mpfPH;
    }

    public void setMpfPH(double mpfPH) {
        this.mpfPH = mpfPH;
    }

    public double getPvPJ() {
        return pvPJ;
    }

    public void setPvPJ(double pvPJ) {
        this.pvPJ = pvPJ;
    }

    public int getIdLavaka() {
        return idLavaka;
    }

    public void setIdLavaka(int idLavaka) {
        this.idLavaka = idLavaka;
    }

    public double getMmitotoPH() {
        return mmitotoPH;
    }

    public void setMmitotoPH(double mmitotoPH) {
        this.mmitotoPH = mmitotoPH;
    }

    public double getMpropGT() {
        return mpropGT;
    }

    public void setMpropGT(double mpropGT) {
        this.mpropGT = mpropGT;
    }

    public double getMduretePH() {
        return mduretePH;
    }

    public void setMduretePH(double mduretePH) {
        this.mduretePH = mduretePH;
    }

    public double getEfficacitePJ() {
        return efficacitePJ;
    }

    public void setEfficacitePJ(double efficacitePJ) {
        this.efficacitePJ = efficacitePJ;
    }

}
