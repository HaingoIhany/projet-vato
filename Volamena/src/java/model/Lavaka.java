/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import utilitaire.DBTable;

/**
 *
 * @author Haingo
 */
public class Lavaka extends DBTable  {
    private int id;
    private int idcarriere;
    private String nom;
    private double x;
    private double y;
    private double rayon;
    private double proportion;

    public Lavaka() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdcarriere() {
        return idcarriere;
    }

    public void setIdcarriere(int idcarriere) {
        this.idcarriere = idcarriere;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getRayon() {
        return rayon;
    }

    public void setRayon(double rayon) {
        this.rayon = rayon;
    }

    public double getProportion() {
        return proportion;
    }

    public void setProportion(double proportion) {
        this.proportion = proportion;
    }

    public Lavaka(int id, int idcarriere, String nom, double x, double y, double rayon, double proportion) {
        this.id = id;
        this.idcarriere = idcarriere;
        this.nom = nom;
        this.x = x;
        this.y = y;
        this.rayon = rayon;
        this.proportion = proportion;
    }

    
}
