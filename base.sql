CREATE DATABASE volamena;
USE volamena;

CREATE TABLE carriere(
	id INT auto_increment PRIMARY KEY,
	nom VARCHAR(30)
);

INSERT INTO carriere VALUES (NULL, 'Carriere1');

CREATE TABLE lavaka(
	id INT auto_increment PRIMARY KEY,
	idcarriere INTEGER NOT NULL,
	nom VARCHAR(30) NOT NULL,
	x double NOT NULL,
	y double NOT NULL,
	rayon double NOT NULL,
	proportion DOUBLE,
	FOREIGN KEY (idcarriere) REFERENCES carriere (id)
);

INSERT INTO lavaka VALUES (NULL, 1, 'lavaka1', 10, 10, 15, 70);
INSERT INTO lavaka VALUES (NULL, 1, 'lavaka2', 85, 10, 15, 50);
INSERT INTO lavaka VALUES (NULL, 1, 'lavaka3', 10, 85, 15, 20);
INSERT INTO lavaka VALUES (NULL, 1, 'lavaka4', 85, 85, 15, 10);

CREATE TABLE resultat (
	id INT auto_increment PRIMARY KEY,
	pf double NOT NULL,
	pv double NOT NULL,
	g double NOT NULL,
	qteinit double NOT NULL,
	idLavaka INTEGER NOT NULL,
	datedebut DATETIME NOT NULL,
	datefin DATETIME NOT NULL,
	duree double NOT NULL,
	fasika double NOT NULL,
	volamena double NOT NULL,
	FOREIGN KEY (idLavaka) REFERENCES lavaka (id)
);

CREATE OR REPLACE VIEW vUnitaire AS
SELECT idlavaka, qteinit, duree, fasika, volamena, (pf*g) pfPH, pv, (qteinit / duree) mitotoPH, ((volamena * 1000) / fasika) propGT, (duree / qteinit) duretePH
FROM resultat;

-- Efficacite par jour
SELECT 	idLavaka, AVG(pfPH) mpfPH, (((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) pvPJ, AVG(mitotoPH) mmitotoPH, AVG(propGT) mpropGT,  
		AVG(duretePH) mduretePH, ((AVG(mitotoPH) * AVG(propGT)) * 8) efficacitePJ, ((((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) - AVG(pfPH)) exploitation
FROM vUnitaire
GROUP BY idLavaka
ORDER BY ((AVG(mitotoPH) * AVG(propGT)) * 8) ASC;

-- Durete vato
SELECT 	idLavaka, AVG(pfPH) mpfPH, (((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) pvPJ, AVG(mitotoPH) mmitotoPH, AVG(propGT) mpropGT,  
		AVG(duretePH) mduretePH, ((AVG(mitotoPH) * AVG(propGT)) * 8) efficacitePJ, ((((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) - AVG(pfPH)) exploitation
FROM vUnitaire
GROUP BY idLavaka
ORDER BY AVG(duretePH) DESC;

-- Proportion % volamena amin ny lavaka
SELECT 	idLavaka, AVG(pfPH) mpfPH, (((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) pvPJ, AVG(mitotoPH) mmitotoPH, AVG(propGT) mpropGT,  
		AVG(duretePH) mduretePH, ((AVG(mitotoPH) * AVG(propGT)) * 8) efficacitePJ, ((((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) - AVG(pfPH)) exploitation
FROM vUnitaire
GROUP BY idLavaka
ORDER BY AVG(propGT) ASC;

-- Exploitation
SELECT 	idLavaka, AVG(pfPH) mpfPH, (((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) pvPJ, AVG(mitotoPH) mmitotoPH, AVG(propGT) mpropGT,  
		AVG(duretePH) mduretePH, ((AVG(mitotoPH) * AVG(propGT)) * 8) efficacitePJ, ((((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) - AVG(pfPH)) exploitation
FROM vUnitaire
GROUP BY idLavaka
ORDER BY ((((AVG(mitotoPH) * AVG(propGT)) * 8) * pv) - AVG(pfPH)) ASC;





-- 3h = 2000kg
-- 1h = ? => (2000 / 3);

-- 1t = 21g
-- 500t = ?g

-- lavaka iray exemple 2t/h ny vototo => manome 21g/t (proportion)  
-- perf => fitotona par heure d'une lavaka ?t/h
-- proportion centrifugeuse (zetina)kg/h
-- Efficacite = MPF + Prop = (?)kg/h => (?)kg/h * 8 = efficacite en moyenne par jour, trier et afficher sur un tableau


-- proportion => volamena par heure ao amle lavaka en g/t


-- Couche 1 => proportion mavo -> mena (ambony ndrindra(pure));
-- Couche 2 => durete (mafy makany malemy) => (malemy) = gris, (mafy) = mainty;
-- Couche 3 => efficacite (mamokatra be indrindra anatin ny iray andro);
